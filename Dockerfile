FROM scratch
EXPOSE 8080
ENTRYPOINT ["/staging-test"]
COPY ./bin/ /